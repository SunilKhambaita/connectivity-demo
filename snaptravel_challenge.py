'''
###############################################################################

#                   SNAPTRAVEL TAKE HOME CHALLENGE
#                       Done by Sunil Khambaita

###############################################################################
'''

import sys, requests, xmltodict, sys, copy, pymongo
from pprint import pprint

LINE_BREAKER = "-------------------------------------------------------------------"

'''
###
### STEP 1 ---- Reading Terminal Arguments
###
'''

def parse_arguments():
    """Parses the arguments in the command line and returns the
    city, check_in and check_out date respectively

    Parameters:
    None:

    Returns:
    string: city
    string: check_in
    string: check_out
   """
    
    # Error checking
    if len(sys.argv) != 4:
        print("To run application: Please enter a city name, check in date and check out date! :)")
        exit()
    
    # Terminal arguments
    city = sys.argv[1]
    check_in = sys.argv[2]
    check_out = sys.argv[3]
    
    return city, check_in, check_out


'''
###
### STEP 2 ---- Making 2 HTTP POST Requests
###
'''

def json_request(city, check_in, check_out):
    """Initiates a JSON HTTP POST request consisting of a city, check in date,
    check out date and returns a list of snaptravel hotels.

    Parameters:
    string: city
    string: check_in
    string: check_out

    Returns:
    List[dict]: snaptravel_hotels
   """
    
    # API
    SNAPTRAVEL_API = 'https://experimentation.getsnaptravel.com/interview/hotels'    
    
    # JSON POST Request
    snaptravel_data = {'city': city, 'checkin': check_in,
                    'checkout': check_out, 'provider': 'snaptravel'}

    # Snaptravel API Request
    snaptravel_request = requests.post(SNAPTRAVEL_API, snaptravel_data)
    snaptravel_response = snaptravel_request.json()
    snaptravel_hotels = snaptravel_response['hotels']
    print("Returned", len(snaptravel_hotels), "responses from SNAPTRAVEL API!")
    print(LINE_BREAKER)
    
    return snaptravel_hotels

def xml_request(city, check_in, check_out):
    """Initiates a XML HTTP POST request consisting of a city, check in date,
    check out date and returns a list of legacy hotels.

    Parameters:
    string: city
    string: check_in
    string: check_out

    Returns:
    List[OrderedDict]: legacy_hotels
   """
    
    # API
    LEGACY_API = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'
    
    # Second XML POST Request.
    legacy_data = '''<?xml version = "1.0" encoding = "UTF-8"?><root><checkin>'''+ \
    check_in +'''</checkin><checkout>'''+ check_out +'''</checkout><city>'''+ city + \
    '''</city><provider>snaptravel</provider></root>'''
    
    # Legacy API Request
    headers = {'Content-Type': 'application/xml'}
    legacy_request = requests.post(LEGACY_API, data = legacy_data, headers = headers)
    legacy_response = legacy_request.text
    
    # Converting XML response to dictionary
    legacy_hotels = xmltodict.parse(legacy_response)['root']['element']
    
    print("Returned", len(legacy_hotels), "responses from LEGACY API!")
    print(LINE_BREAKER)
    
    return legacy_hotels


'''
###
### STEP 3 ---- Merging responses + storing response to MongoDB
###
'''

def merge_responses(snaptravel_hotels, legacy_hotels):
    """Merges responses from the snaptravel hotel API and the legacy hotel API.
    Only the responses which have a match based off the IDs will be merged.

    Parameters:
    List[dict]: snaptravel_hotels
    List[OrderedDict]: legacy_hotels

    Returns:
    List[dict]: merged_hotels
   """

    # Data that will be going to MongoDB
    merged_hotels = []
    
    # Caching legacy results by ID and its price for quicker lookup later O(1)
    legacy_storage = {}
    
    # Storing legacy results by ID and Price
    for i in range(len(legacy_hotels)):
        legacy_id = legacy_hotels[i]['id']
        legacy_price = legacy_hotels[i]['price']
        legacy_storage[legacy_id] = legacy_price
    
    # Finding matching IDs on Snaptravel hotels with Legacy IDs.
    for i in range(len(snaptravel_hotels)):
        snaptravel_id = snaptravel_hotels[i]['id']
        snaptravel_price = snaptravel_hotels[i]['price']
        # We have a match!
        if str(snaptravel_id) in legacy_storage:
            temp = copy.deepcopy(snaptravel_hotels[i])
            # Creating a new 'prices' key
            temp['prices'] = {'snaptravel': snaptravel_price, 'retail' : legacy_storage[str(snaptravel_id)]}
            # Removing the old one 'price'
            del temp['price']
            merged_hotels.append(temp)
    
    print("Found", len(merged_hotels), "matching IDs on SnapTravel API and Legacy API.")
    print(LINE_BREAKER)    
    return merged_hotels

def store_merged_hotels(merged_hotels):
    """Stores merged hotels into a MongoDB database.

    Parameters:
    List[dict]: merged_hotels

    Returns:
    None : None
    """
    
    # Inserting results into a database
    my_client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
    my_db = my_client["snaptravel"]
    snap_retail = my_db["snapvsretail"]    
    
    # Only do an insertion if there's a match
    if len(merged_hotels) > 0:
        try:
            # Starting with a fresh database entry
            snap_retail.delete_many({})            
            snap_retail.insert_many(merged_hotels)
            print ("Inserted", len(merged_hotels), "entries into MongoDB! Here are the IDs and Prices:")
            print(LINE_BREAKER)            
            
            for results in snap_retail.find({}, {"_id": 0, "id": 1, "name": 1, "prices": 1 }):
                print(results)
            print(LINE_BREAKER)
            
            
        except Exception as exc:
            print("Exception: " + str(exc) + " --- Please confirm Mongo is up!")
            print(LINE_BREAKER)            
            
if __name__ == "__main__":
    
    # Step 1 - Parsing
    city, check_in, check_out = parse_arguments()
    
    # Step 2 - API Requests
    snaptravel_hotels = json_request(city, check_in, check_out)
    legacy_hotels = xml_request(city, check_in, check_out)
    
    # Step 3 - Merging and Storing
    merged_hotels = merge_responses(snaptravel_hotels, legacy_hotels)
    store_merged_hotels(merged_hotels)
    
    print("Thank you for your time!")
    print(LINE_BREAKER)
    