# Connectivity Demo
Hi SnapTravel! This is my terminal application written in Python. It's designed to send 2 API Requests and store the intersecting results in MongoDB.

# Requirements in order to run my application

There’s a few things you would need to have installed in order run my application:

### Number 1 - Terminal
```
I hope you have this in your computer otherwise it'll be really hard to run my application! :/
```

### Number 2 - PIP
```
Pip is a package management system which is required to install the remaining things in this list. So just incase you don't have it. Try running this command:

- sudo easy_install pip
```

### Number 3 - Requests
```
Requests is a Python HTTP library built by Apache. It is simple, elegant and built for human beings.

- pip install requests

```

### Number 4 - XMLToDict
```
This is a Python library which helps us to work with XML objects. Especially from the XML POST Request in the second requirement. It gives us the feeling as if we're dealing with a simple dictionary which simplifies a lot of our code. To install this beauty:

- pip install xmltodict

```

### Number 5 - PyMongo
```
Since we will be storing our merged results in a MongoDB database. We will need a driver which allows us to use this with Python. And what better way than using PyMongo (the official driver designed by Mongo Developers themselves.)

- pip install pymongo

```

And that's it for installation you're all done!

# How to use my application

Please go to terminal. Switch into my directory where my ```snaptravel_challenge.py``` file is located. Then run a command which is something similar to this:

```
python snaptravel_challenge.py Toronto 2019-08-29 2019-08-30

```

You'll get a response with how many results were found on both APIs. How many joins and insertions that occured into my Database.

Please note: Joins and Insertions will only happen if two IDs of both APIs are the same.

I hope you enjoy playing with this just as much as I did making it!

Thank you!!

* If you run into any technical difficulties contact sunilkhambaita@gmail.com