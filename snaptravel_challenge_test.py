import unittest
from snaptravel_challenge import *

city = "Toronto"
check_in = "2019-08-28"
check_out = "2019-08-30"  

class TestSnaptravelChallenge(unittest.TestCase):
    
    # Testing if JSON request gives a response
    def test_json_request_1(self):
        
        actual = json_request(city, check_in, check_out)
        number_of_results = len(actual)
        
        self.assertGreater(number_of_results, 0)
        
    # Testing if XML request gives a response
    def test_xml_request_1(self):
        
        actual = xml_request(city, check_in, check_out)
        number_of_results = len(actual)
        
        self.assertGreater(number_of_results, 0)    

if __name__ == '__main__':
    unittest.main()


